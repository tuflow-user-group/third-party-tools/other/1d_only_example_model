# 1D_Only_Example_Model

This is an example of a TUFLOW setup to allow a 1D only model.  As it is not possible to have a 1D Only model within TUFLOW, a small dummy 2D error is specified within the TGC.  

## TGC
The TUFLOW Geometry Control file is configured to set up a small 2D model area with a few cells with a uniform elevation and roughness.  The 2D domain has no boundary conditions associated with it.  It should be possible to use this TGC without further amendment.

## TBC

The TUFLOW Boundary Contol (TBC) File is blank as the 2D domain is a dummy section there are no 2D Boundaries. The TBC file can be used with no further amendments.

## ECF
The Estry Control File (ECF) is currently blank but this is where you would add the definition of the 1D network.

## TCF
The TUFLOW Control File (TCF) is set up to read the relevant files.  It requires updating to refer to the correct bc_dbase files, materials files as well as the relevant run parameters.


